<?php

namespace App\Providers;

use App\Filters\Event\EventFilter;
use Illuminate\Support\ServiceProvider;

class ModelFilterServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('event_filter', function () {
            return new EventFilter();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}