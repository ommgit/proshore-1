<?php

namespace App\Filters\Event;


use App\Models\Event;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class EventFilter
{
    /**
     * @param Event $event
     * @param array $filterParams
     * @return LengthAwarePaginator|Builder[]|Collection
     */
    public static function apply(Event $event, array $filterParams)
    {
        $event = ($event)->newQuery();
        $event->with(['organizer:id,name,email,phone,website_link', 'category:id,name'])
            ->when(isset($filterParams['title']), function ($query) use ($filterParams) {
                $event_number = $filterParams['title'];
                return $query->where('title', 'like', "%$event_number%");
            })->when(isset($filterParams['event_category_id']), function ($query) use ($filterParams) {
                $eventCategoryID = $filterParams['event_category_id'];
                return $query->where('event_category_id', $eventCategoryID);
            })->when(isset($filterParams['event_organizer_id']), function ($query) use ($filterParams) {
                $eventOrganizerID = $filterParams['event_organizer_id'];
                return $query->where('event_organizer_id', $eventOrganizerID);
            })->when(isset($filterParams['event_happening']), function ($query) use ($filterParams) {
                $eventHappening = $filterParams['event_happening'];
                switch ($eventHappening) {
                    case 'upcoming':
                        return $query->upcoming();
                    case 'finished' :
                        return $query->finished();
                    case 'upcoming_in_next_week' :
                        return $query->upcomingInDays(7);
                    case 'finished_last_week':
                        return $query->finishedForDays(7);
                    case 'today':
                        return $query->today();
                    case 'tomorrow':
                        return $query->tomorrow();
                    default:
                        break;
                }
                return $query;
            });

        $event = (isset($filterParams['sort_by_start_date'])
            && in_array($filterParams['sort_by_start_date'], ['asc', 'desc'])
        )
            ? $event->orderBy('start_date', $filterParams['sort_by_start_date'])
            : $event->orderBy('id', 'DESC');

        return isset($filterParams['records_per_page'])
            ? $event->paginate($filterParams['records_per_page'])
            : $event->get();
    }

}
