<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

class Organizer extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'event_organizers';

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'slug',
        'email',
        'phone',
        'website_link'
    ];

    /**
     * @return void
     */
    public static function boot(): void
    {
        parent::boot();

        static::creating(function ($model) {
            $model->slug = Str::slug($model->name).'-'.rand(10,20).time();
        });
    }

    /**
     * @return HasMany
     */
    public function events(): HasMany
    {
        return $this->hasMany(Event::class, 'event_organizer_id');
    }


}
