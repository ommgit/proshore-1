<?php


namespace App\Models;

use App\Traits\FileStorage;
use App\Traits\HasEventScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Str;

class Event extends Model
{
    use HasFactory, HasEventScope, FileStorage;

    const IMAGE_UPLOAD_FOLDER = "events/";
    /**
     * @var string
     */
    protected $table = 'events';
    /**
     * @var string[]
     */
    protected $fillable = [
        'title',
        'slug',
        'description',
        'image',
        'start_date',
        'end_date',
        'event_category_id',
        'event_organizer_id'
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'start_date' => 'date',
        'end_date' => 'date'
    ];

    /**
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->slug = Str::slug($model->title) . '-' . rand(10, 20) . time();
        });

        static::updating(function ($model) {
            $model->slug = Str::slug($model->title) . '-' . rand(10, 20) . time();
        });
    }

    /**
     * @param string $image
     * @return string
     */
    public function getImagePath(string $imageName): string
    {
        return $this->getFileUrl(self::IMAGE_UPLOAD_FOLDER,$imageName);
    }

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'event_category_id');
    }

    /**
     * @return BelongsTo
     */
    public function organizer(): BelongsTo
    {
        return $this->belongsTo(Organizer::class, 'event_organizer_id');
    }


}
