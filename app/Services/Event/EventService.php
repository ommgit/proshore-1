<?php

namespace App\Services\Event;

use App\Models\Event;
use App\Repositories\Event\EventRepository;
use App\Traits\FileStorage;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventService
{
    use FileStorage;

    protected EventRepository $eventRepo;

    /**
     * @param EventRepository $eventRepo
     */
    public function __construct(EventRepository $eventRepo)
    {
        $this->eventRepo = $eventRepo;
    }

    public function getEvents(Request $request)
    {
        $filterParams = [
            'title' => $request->get('title'),
            'event_category_id' => $request->get('event_category_id'),
            'event_organizer_id' => $request->get('event_organizer_id'),
            'event_happening' => $request->get('event_happening'),
            'records_per_page' => $request->has('records_per_page')
                ? $request->get('records_per_page') : 10,
            'sort_by_start_date' => $request->get('sort_by_start_date')
        ];
        return $this->eventRepo->getEvents($filterParams);
    }

    /**
     * @param $validatedData
     * @return mixed
     * @throws Exception
     */
    public function saveEvent(array $validatedData): mixed
    {
        $imageRequest = false;
        try {
            DB::beginTransaction();
            $imageRequest = array_key_exists('image', $validatedData) & isset($validatedData['image']);
            // check if the request has file to be uploaded
            if ($imageRequest) {
                $fileUploadFolder = Event::IMAGE_UPLOAD_FOLDER;
                $uploadedImage = $validatedData['image'];
                $fileName = $this->saveFile($fileUploadFolder, $uploadedImage);
                $validatedData['image'] = $fileName;
            }
            $event = $this->eventRepo->saveEvent($validatedData);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            // deleting the uploaded file if exception occurs when file is present in the request
            if ($imageRequest) {
                $this->deleteFile($fileUploadFolder, $fileName);
            }
            throw $exception;
        }
        return $event;
    }

    /**
     * @param int $eventID
     * @return mixed
     * @throws Exception
     */
    public function showEventDetails(int $eventID): mixed
    {
        return $this->eventRepo->getEventByIDOrFail(
            $eventID,
            ['*'],
            [
                'category:id,name',
                'organizer:id,name,email,phone,website_link'
            ]
        );
    }

    /**
     * @param int $eventID
     * @param array $validatedData
     * @return mixed
     * @throws Exception
     */
    public function updateEvent(int $eventID, array $validatedData): mixed
    {
        $imageRequest = false;
        try {
            DB::beginTransaction();
            $imageRequest = array_key_exists('image', $validatedData) & isset($validatedData['image']);
            // check if the request has file to be uploaded
            if ($imageRequest) {
                $fileUploadFolder = Event::IMAGE_UPLOAD_FOLDER;
                $uploadedImage = $validatedData['image'];
                //delete old image before storing new
                $this->deleteFile($fileUploadFolder, $uploadedImage);
                //storing new imager
                $fileName = $this->saveFile($fileUploadFolder, $uploadedImage);
                $validatedData['image'] = $fileName;
            }
            $event = $this->eventRepo->updateEvent($eventID, $validatedData);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            // deleting the uploaded file if exception occurs when file is present in the request
            if ($imageRequest) {
                $this->deleteFile($fileUploadFolder, $fileName);
            }
            throw $exception;
        }
        return $event;
    }

    /**
     * @param int $eventID
     * @return mixed
     * @throws Exception
     */
    public function deleteEvent(int $eventID): mixed
    {
        try {
            DB::beginTransaction();
            $event = $this->eventRepo->deleteEvent($eventID);
            // deleting the uploaded file if exception occurs when file is present in the request
            $fileUploadFolder = Event::IMAGE_UPLOAD_FOLDER;
            $this->deleteFile($fileUploadFolder, $event->image);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        return $event;
    }

}