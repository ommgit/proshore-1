<?php

namespace App\Repositories\Event;

use App\Models\Event;
use EventFilter;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EventRepository
{
    /**
     * @var Event
     */
    protected $event;

    /**
     * @param Event $event
     */
    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    /**
     * @param array $filterParams
     * @return mixed
     */
    public function getEvents(array $filterParams): mixed
    {
        return EventFilter::apply($this->event, $filterParams);
    }

    /**
     * @param int $eventID
     * @param array $selects
     * @param array $with
     * @return mixed
     */
    public function getEventByID(int $eventID, array $selects = ["*"], array $with = []): mixed
    {
        return $this->event->where('id', $eventID)
            ->select($selects)
            ->with($with)
            ->first();
    }

    /**
     * @param int $eventID
     * @param array $selects
     * @param array $with
     * @return mixed
     * @throws Exception
     */
    public function getEventByIDOrFail(int $eventID, array $selects = ["*"], array $with = []): mixed
    {
        $event = $this->event->where('id', $eventID)
            ->select($selects)
            ->with($with)
            ->first();
        if (!$event) {
            throw new ModelNotFoundException('No Such Event Found', 404);
        }
        return $event;
    }

    /**
     * @param string $slug
     * @param array $selects
     * @param array $with
     * @return mixed
     * @throws Exception
     */
    public function getEventBySlug(string $slug, array $selects = ["*"], array $with = []): mixed
    {
        $event = $this->event->where('slug', $slug)
            ->select($selects)
            ->with($with)
            ->first();
        if (!$event) {
            throw new ModelNotFoundException('No Such Event Found', 404);
        }
        return $event;
    }

    /**
     * @param array $validatedData
     * @return mixed
     */
    public function saveEvent(array $validatedData): mixed
    {
        return $this->event->create($validatedData)->fresh();
    }

    /**
     * @param int $eventID
     * @param array $validatedData
     * @return mixed
     * @throws Exception
     */
    public function updateEvent(int $eventID, array $validatedData): mixed
    {
        $event = $this->getEventByIDOrFail($eventID);
        $event->update($validatedData);
        return $event;
    }

    /**
     * @param int $eventID
     * @return mixed
     * @throws Exception
     */
    public function deleteEvent(int $eventID): mixed
    {
        $event = $this->getEventByIDOrFail($eventID);
        $event->delete();
        return $event;
    }


}