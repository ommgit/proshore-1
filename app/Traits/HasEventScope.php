<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

trait HasEventScope
{

    public function scopeUpcoming($query): Builder
    {
        return $query->where('start_date', '>', Carbon::today());
    }

    public function scopeFinished($query): Builder
    {
        return $query->where('end_date', '<', Carbon::today());
    }

    public function scopeUpcomingInDays($query, int $days): Builder
    {
        return $query->where('start_date', '>', Carbon::today())
            ->where('start_date', '<=', Carbon::today()->addDays($days));
    }

    public function scopeFinishedForDays($query, int $days): Builder
    {
        return $query->where('end_date', '<', Carbon::today())
            ->where('end_date', '>=', Carbon::today()->subDays($days));
    }

    public function scopeToday($query): Builder
    {
        return $query->where('start_date', '=', Carbon::today());
    }

    public function scopeTomorrow($query): Builder
    {
        return $query->where('start_date', '=', Carbon::tomorrow());
    }
}