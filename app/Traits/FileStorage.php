<?php

namespace App\Traits;

use Exception;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait FileStorage
{

    /**
     * @param $filePath
     * @param $fileName
     * @return string
     */
    public function getFileUrl($filePath, $fileName): string
    {
        $disk = config('filesystems.default');
        if ($disk == 'public' || $disk == 'local') {
            return asset('storage/' . $filePath . $fileName);
        }
        return Storage::disk(config('filesystems.default'))->url($filePath . $fileName);
    }

    /**
     * @param $filePath
     * @param $file
     * @return bool
     */
    public function deleteFile($filePath, $file): bool
    {
        $fileExistsInStorage = Storage::disk(config('filesystems.default'))->exists($filePath . $file);
        if ($fileExistsInStorage) {
            return Storage::disk(config('filesystems.default'))->delete($filePath . $file);
        }
        return false;
    }


    /**
     * @param string $pathToSave
     * @param UploadedFile $file
     * @return string
     * @throws Exception
     */
    public function saveFile(string $pathToSave, UploadedFile $file): string
    {
            $fileNameWithExt = $file->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $file->getClientOriginalExtension();
            $fileNameToStore = $fileName . '_' . time() . '.' . $extension;
            Storage::disk(config('filesystems.default'))->putFileAs($pathToSave, $file, $fileNameToStore);
            return $fileNameToStore;
    }

}