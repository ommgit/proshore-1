<?php

namespace App\Facades\Event;

use Illuminate\Support\Facades\Facade;

class EventFilterFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'event_filter';
    }
}