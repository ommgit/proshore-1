<?php

namespace App\Http\Resources\Event\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'image' => $this->resource->getImagePath($this->image),
            'category' => $this->category->name,
            'organizer' => $this->organizer->name,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date
        ];
    }
}
