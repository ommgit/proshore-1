<?php

namespace App\Http\Resources\Event\Admin;

use App\Http\Resources\Organizer\OrganizerResource;
use Illuminate\Http\Resources\Json\JsonResource;

class EventDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'slug' => $this->slug,
            'description' => $this->description,
            'image' => $this->resource->getImagePath($this->image),
            'category' => $this->category->name,
            'organizer' => new OrganizerResource($this->organizer),
            'start_date' => $this->start_date,
            'end_date' => $this->end_date
        ];
    }
}
