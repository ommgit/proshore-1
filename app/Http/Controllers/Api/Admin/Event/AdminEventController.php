<?php

namespace App\Http\Controllers\Api\Admin\Event;

use App\Http\Controllers\Controller;
use App\Http\Requests\Event\EventCreateRequest;
use App\Http\Requests\Event\EventUpdateRequest;
use App\Http\Resources\Event\Admin\EventCollection;
use App\Http\Resources\Event\Admin\EventDetailResource;
use App\Http\Resources\Event\Admin\EventResource;
use App\Services\Event\EventService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AdminEventController extends Controller
{
    /**
     * @var EventService
     */
    protected $eventService;

    /**
     * @param EventService $eventService
     */
    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * Display a listing of the events.
     * @param Request
     * @return JsonResponse|EventCollection
     */
    public function index(Request $request): JsonResponse|EventCollection
    {
        try{
            $events = $this->eventService->getEvents($request);
            return (new EventCollection($events))->preserveQuery();
        }catch (Exception $e){
            return respondError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * Store a newly created event.
     *
     * @param EventCreateRequest $request
     * @return JsonResponse
     */
    public function store(EventCreateRequest $request): JsonResponse
    {
        try {
            $validatedData = $request->validated();
            $event = $this->eventService->saveEvent($validatedData);
            $event = new EventResource($event);
            return respondSuccess('Event saved successfully', $event);
        } catch (Exception $e) {
            return respondError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Display the specified event.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        try {
            $event = $this->eventService->showEventDetails($id);
            $event = new EventDetailResource($event);
            return respondSuccess('Event details found', $event);
        } catch (Exception $e) {
            return respondError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Update the specified event in storage.
     *
     * @param EventUpdateRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(EventUpdateRequest $request, int $id): JsonResponse
    {
        try {
            $validatedData = $request->validated();
            $event = $this->eventService->updateEvent($id, $validatedData);
            $event = new EventResource($event);
            return respondSuccess('Event updated successfully', $event);
        } catch (Exception $e) {
            return respondError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Remove the specified event from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        try {
            $event = $this->eventService->deleteEvent($id);
            $event = new EventResource($event);
            return respondSuccess('Event deleted successfully', $event);
        } catch (Exception $e) {
            return respondError($e->getMessage(), $e->getCode());
        }
    }
}
