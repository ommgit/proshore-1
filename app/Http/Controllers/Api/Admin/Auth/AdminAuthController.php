<?php

namespace App\Http\Controllers\Api\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserResource;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminAuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        try {
            if (Auth::attempt([
                'email' => $request->email,
                'password' => $request->password,
                'is_admin' => 1
            ])) {
                $user = auth()->user();
                $data['user'] = new UserResource($user);
                $data['access_token'] = $user->createToken('Personal Access Token')->accessToken;
                return respondSuccess('Authenticated', $data);
            } else {
                throw new Exception('Invalid Login Credentials', 401);
            }
        } catch (Exception $e) {
            return respondError($e->getMessage(), $e->getCode());
        }
    }

    public function logout()
    {
        try {
            $accessToken = auth()->user()->token();
            $accessToken->revoke();
            return respondSuccess('Logout Successful');
        } catch (Exception $e) {
            return respondError('Could not logout the user : Something Went Wrong !', $e->getCode());
        }
    }
}