<?php

namespace App\Http\Controllers\Api\Front\Event;

use App\Http\Controllers\Controller;
use App\Services\Event\EventService;
use Illuminate\Http\Request;

class EventController extends Controller
{
    protected EventService $eventService ;

    /**
     * @param EventService $eventService
     */
    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPublishedEvents(Request $request)
    {
        return respondSuccess('Events Found');
    }
}
