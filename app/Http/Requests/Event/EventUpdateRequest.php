<?php

namespace App\Http\Requests\Event;

use Illuminate\Foundation\Http\FormRequest;

class EventUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => ['bail', 'sometimes', 'required', 'max:191'],
            'description' => ['bail', 'sometimes', 'required'],
            'image' => ['bail', 'sometimes', 'required', 'max:2048', 'mimes:jpg,jpeg,png,webp'],
            'event_category_id' => ['bail', 'sometimes', 'required', 'exists:event_categories,id'],
            'event_organizer_id' => ['bail', 'sometimes', 'required', 'exists:event_organizers,id'],
            'start_date' => [
                'bail',
                'sometimes',
                'required',
                'date_format:Y-m-d',
                'after_or_equal:' . date('Y-m-d')
            ],
            'end_date' => ['bail', 'sometimes', 'required', 'date_format:Y-m-d', 'after_or_equal:start_date'],
        ];
    }
}
