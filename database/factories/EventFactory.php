<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Event;
use App\Models\Organizer;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory
 */
class EventFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $eventName = fake()->name;
        $startDate = fake()->dateTimeBetween('next Monday', 'next Monday +10 days');
        $endDate = fake()->dateTimeBetween($startDate, $startDate->format('Y-m-d H:i:s') . ' +12 days');
        $eventCategory = Category::factory()->create();
        $eventOrganizer = Organizer::factory()->create();

        return [
            'title' => $eventName,
            'description' => fake()->text(300),
            'image' => fake()->image('public/storage/events', 640, 480, null, false),
            'start_date' => $startDate,
            'end_date' => $endDate,
            'event_category_id' => $eventCategory->id,
            'event_organizer_id' => $eventOrganizer->id
        ];
    }
}
