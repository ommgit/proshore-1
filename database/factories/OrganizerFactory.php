<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Organizer>
 */
class OrganizerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $name = fake()->name;
        return [
            'name' => $name,
            'email' => fake()->safeEmail(),
            'phone' => fake()->numerify('##-#######'),
            'website_link' => strtolower(str_replace(' ', '', $name)).'.com',
        ];
    }
}
