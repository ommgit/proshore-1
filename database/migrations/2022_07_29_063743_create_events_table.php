<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug')->unique();
            $table->longText('description');
            $table->string('image');
            $table->date('start_date')->index('start_date_event_index');
            $table->date('end_date')->index('end_date_event_index');
            $table->unsignedBigInteger('event_category_id');
            $table->unsignedBigInteger('event_organizer_id');
            $table->foreign('event_category_id')->references('id')->on('event_categories');
            $table->foreign('event_organizer_id')->references('id')->on('event_organizers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
};
