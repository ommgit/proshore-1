<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::updateOrCreate([
            'email' => 'test@admin.com'
        ], [
            'name' => 'Admin',
            'password' => bcrypt('admin123'),
            'is_admin' => 1
        ]);
    }
}
