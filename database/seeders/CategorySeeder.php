<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $eventCategories = ['Workshop', 'Training', 'Trip', 'Festivals', 'Concert', 'Arts', 'Comedy Show'];
        $categoryNamesFunc = function ($value) {
            return [
                'name' => $value,
                'slug' => Str::slug($value)
            ];
        };
        $categoryData = array_map($categoryNamesFunc, $eventCategories);

        Category::upsert($categoryData,'name');
    }
}
