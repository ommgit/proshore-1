<?php

use App\Http\Controllers\Api\Admin\Auth\AdminAuthController;
use App\Http\Controllers\Api\Admin\Event\AdminEventController;
use App\Http\Controllers\Api\Front\Event\EventController;
use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return respondSuccess('User Details', new UserResource(request()->user()));
});

//admin login
Route::controller(AdminAuthController::class)->prefix('admin')->group(function () {
    Route::post('/login', 'login');
    Route::post('/logout', 'logout')->middleware('auth:api');
});


//events - admin

Route::middleware(['auth:api','auth.admin'])->prefix('admin')->group(function () {
    Route::apiResource('events', AdminEventController::class);
});


////events - front
//Route::controller(EventController::class)->group(function () {
//    Route::get('events', 'getPublishedEvents');
//});